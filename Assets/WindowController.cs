﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowController : MonoBehaviour
{
    public GameObject[] PartsParents;
    private int partsDone = 0;

    public void AddPart()
    {
        partsDone++;
        if (partsDone == 4)
        {
            foreach (var partsParent in PartsParents)
            {
                partsParent.SetActive(false);
            }
            gameObject.SetActive(true);
        }
    }

}
