﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class TranslateUp : MonoBehaviour
{
    public void Translate(object o, ControllableEventArgs e)
    {
        transform.position = new Vector3(transform.position.x, e.value / 500, transform.position.z);
    }
}
